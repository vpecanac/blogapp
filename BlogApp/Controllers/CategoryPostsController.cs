﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogApp.Data;
using BlogApp.Models;

namespace BlogApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CategoryPostsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoryPostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CategoryPosts
        [HttpGet]
        public IEnumerable<CategoryPost> GetCategoryPosts()
        {
            return _context.CategoryPosts;
        }

        // GET: api/CategoryPosts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategoryPost([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var categoryPost = await _context.CategoryPosts.FindAsync(id);

            if (categoryPost == null)
            {
                return NotFound();
            }

            return Ok(categoryPost);
        }

        // PUT: api/CategoryPosts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategoryPost([FromRoute] string id, [FromBody] CategoryPost categoryPost)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categoryPost.Id)
            {
                return BadRequest();
            }

            _context.Entry(categoryPost).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryPostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CategoryPosts
        [HttpPost]
        public async Task<IActionResult> PostCategoryPost([FromBody] CategoryPost categoryPost)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CategoryPosts.Add(categoryPost);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCategoryPost", new { id = categoryPost.Id }, categoryPost);
        }

        // DELETE: api/CategoryPosts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategoryPost([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var categoryPost = await _context.CategoryPosts.FindAsync(id);
            if (categoryPost == null)
            {
                return NotFound();
            }

            _context.CategoryPosts.Remove(categoryPost);
            await _context.SaveChangesAsync();

            return Ok(categoryPost);
        }

        private bool CategoryPostExists(string id)
        {
            return _context.CategoryPosts.Any(e => e.Id == id);
        }
    }
}