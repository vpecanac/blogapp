﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogApp.Data;
using BlogApp.Models;

namespace BlogApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class TagPostsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TagPostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TagPosts
        [HttpGet]
        public IEnumerable<TagPost> GetTagPosts()
        {
            return _context.TagPosts;
        }

        // GET: api/TagPosts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTagPost([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tagPost = await _context.TagPosts.FindAsync(id);

            if (tagPost == null)
            {
                return NotFound();
            }

            return Ok(tagPost);
        }

        // PUT: api/TagPosts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTagPost([FromRoute] string id, [FromBody] TagPost tagPost)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tagPost.Id)
            {
                return BadRequest();
            }

            _context.Entry(tagPost).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TagPostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TagPosts
        [HttpPost]
        public async Task<IActionResult> PostTagPost([FromBody] TagPost tagPost)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TagPosts.Add(tagPost);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTagPost", new { id = tagPost.Id }, tagPost);
        }

        // DELETE: api/TagPosts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTagPost([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tagPost = await _context.TagPosts.FindAsync(id);
            if (tagPost == null)
            {
                return NotFound();
            }

            _context.TagPosts.Remove(tagPost);
            await _context.SaveChangesAsync();

            return Ok(tagPost);
        }

        private bool TagPostExists(string id)
        {
            return _context.TagPosts.Any(e => e.Id == id);
        }
    }
}