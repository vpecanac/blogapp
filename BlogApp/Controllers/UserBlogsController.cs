﻿using BlogApp.Data;
using BlogApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UserBlogsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UserBlogsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/UserBlogs
        [HttpGet]
        public IEnumerable<UserBlog> GetUserBlogs()
        {
            return _context.UserBlogs;
        }

        // GET: api/UserBlogs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserBlog([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userBlog = await _context.UserBlogs.FindAsync(id);

            if (userBlog == null)
            {
                return NotFound();
            }

            return Ok(userBlog);
        }

        // PUT: api/UserBlogs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserBlog([FromRoute] string id, [FromBody] UserBlog userBlog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userBlog.Id)
            {
                return BadRequest();
            }

            _context.Entry(userBlog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserBlogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserBlogs
        [HttpPost]
        public async Task<IActionResult> PostUserBlog([FromBody] UserBlog userBlog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UserBlogs.Add(userBlog);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserBlog", new { id = userBlog.Id }, userBlog);
        }

        // DELETE: api/UserBlogs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserBlog([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userBlog = await _context.UserBlogs.FindAsync(id);
            if (userBlog == null)
            {
                return NotFound();
            }

            _context.UserBlogs.Remove(userBlog);
            await _context.SaveChangesAsync();

            return Ok(userBlog);
        }

        private bool UserBlogExists(string id)
        {
            return _context.UserBlogs.Any(e => e.Id == id);
        }
    }
}