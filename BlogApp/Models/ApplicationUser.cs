﻿using Microsoft.AspNetCore.Identity;

namespace BlogApp.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
    }
}