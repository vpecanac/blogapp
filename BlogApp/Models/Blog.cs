﻿using System;

namespace BlogApp.Models
{
    public class Blog
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public bool AllowComments { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}