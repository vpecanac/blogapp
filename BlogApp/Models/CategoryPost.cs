﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BlogApp.Models
{
    public class CategoryPost
    {
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string PostId { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }

        [ForeignKey("PostId")]
        public Post Post { get; set; }
    }
}