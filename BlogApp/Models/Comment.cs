﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BlogApp.Models
{
    public class Comment
    {
        public string Id { get; set; }
        public string ParentCommentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HomePage { get; set; }
        public string Ip { get; set; }
        public string Text { get; set; }
        public string CreatedAt { get; set; }
        public string ModifiedAt { get; set; }

        [ForeignKey("ParentCommentId")]
        public Comment ParentComment { get; set; }
    }
}