﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BlogApp.Models
{
    public class TagPost
    {
        public string Id { get; set; }
        public string TagId { get; set; }
        public string PostId { get; set; }

        [ForeignKey("TagId")]
        public Tag Tag { get; set; }

        [ForeignKey("PostId")]
        public Post Post { get; set; }
    }
}