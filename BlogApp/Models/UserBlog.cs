﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BlogApp.Models
{
    public class UserBlog
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string PostId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [ForeignKey("PostId")]
        public Post Post { get; set; }
    }
}